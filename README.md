﻿# Smart Search Algorithm Project


<img src="Capture.PNG" title="UI sample"/>

<img src="Capture2.PNG" title="UI sample"/>

<img src="smart_search_algorithms_project.png" title="UI sample"/>


In this project a GUI for a snake puzzle game.
The goal is to put all pieces in the right place to complete the grid and the snake.

- I have implemented 4 search algorithms to solve the puzzle automatically, no matter how hard is the level, and they are:
  - UCS: Unified cost search
  - DFS: Depth-first search
  - BFS: Breadth-first search
  - A*: (A star) algorithm which has the best performance.
- You can select the algorithm and see its performance (total time needed) in the console interface.
- You can build your own levels, by creating new input files as the format of the **level1.txt** format
- You can try to solve the puzzle yourself

## This Repository contains:
- Source Code (Java & JavaFx)
- Game Assets
- Images for a sample output
- Image for UML Class Diagram

**_Rasoul Abouassaf - AI Engineer_**