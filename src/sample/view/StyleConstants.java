package sample.view;

import javafx.scene.image.Image;
import sample.model.SquaresEnum;

import java.util.HashMap;

public class StyleConstants {
    public static HashMap<SquaresEnum, String> squaresIcons = new HashMap<>();

    private static final String HEAD_ICON = "file:resources/images/snake1/selected.png";
    private static final String SNAKE_HEAD_RIGHT = "file:resources/images/snake1/snake_head_right.png";
    private static final String SNAKE_HEAD_LEFT = "file:resources/images/snake1/snake_head_left.png";
    private static final String SNAKE_HEAD_DOWN = "file:resources/images/snake1/snake_head_down.png";
    private static final String SNAKE_HEAD_UP = "file:resources/images/snake1/snake_head_up.png";
    private static final String SNAKE_TAIL_RIGHT = "file:resources/images/snake1/snake_tail_right.png";
    private static final String SNAKE_TAIL_LEFT = "file:resources/images/snake1/snake_tail_left.png";
    private static final String SNAKE_TAIL_DOWN = "file:resources/images/snake1/snake_tail_down.png";
    private static final String SNAKE_TAIL_UP = "file:resources/images/snake1/snake_tail_up.png";
    private static final String SNAKE_RL = "file:resources/images/snake1/snake_body_right_left.png";
    private static final String SNAKE_RD = "file:resources/images/snake1/snake_body_right_down.png";
    private static final String SNAKE_RU = "file:resources/images/snake1/snake_body_right_up.png";
    private static final String SNAKE_LD = "file:resources/images/snake1/snake_body_left_down.png";
    private static final String SNAKE_LU = "file:resources/images/snake1/snake_body_left_up.png";
    private static final String SNAKE_DU = "file:resources/images/snake1/snake_body_down_up.png";

    private static String STONE_ICON = "file:resources/Images/stone.png";
    private static String GLASS_ICON = "file:resources/Images/glass.png";
    private static String VOID_ICON = "file:resources/Images/void.png";

    private static HashMap<String, SquaresEnum> snakeSquares = new HashMap<>();

    public static void initialize() {
        squaresIcons.put(SquaresEnum.SnakeHeadRight, SNAKE_HEAD_RIGHT);
        squaresIcons.put(SquaresEnum.SnakeHeadLeft, SNAKE_HEAD_LEFT);
        squaresIcons.put(SquaresEnum.SnakeHeadDown, SNAKE_HEAD_DOWN);
        squaresIcons.put(SquaresEnum.SnakeHeadUp, SNAKE_HEAD_UP);

        squaresIcons.put(SquaresEnum.SnakeTailRight, SNAKE_TAIL_RIGHT);
        squaresIcons.put(SquaresEnum.SnakeTailLeft, SNAKE_TAIL_LEFT);
        squaresIcons.put(SquaresEnum.SnakeTailDown, SNAKE_TAIL_DOWN);
        squaresIcons.put(SquaresEnum.SnakeTailUp,   SNAKE_TAIL_UP);
        squaresIcons.put(SquaresEnum.SnakeRL, SNAKE_RL);
        squaresIcons.put(SquaresEnum.SnakeRD, SNAKE_RD);
        squaresIcons.put(SquaresEnum.SnakeRU, SNAKE_RU);
        squaresIcons.put(SquaresEnum.SnakeLD, SNAKE_LD);
        squaresIcons.put(SquaresEnum.SnakeLU, SNAKE_LU);
        squaresIcons.put(SquaresEnum.SnakeDU, SNAKE_DU);
        squaresIcons.put(SquaresEnum.Stone, STONE_ICON);
        squaresIcons.put(SquaresEnum.Glass, GLASS_ICON);
        squaresIcons.put(SquaresEnum.Void, VOID_ICON);
        squaresIcons.put(SquaresEnum.Head, HEAD_ICON);

        snakeSquares.put("1000H", SquaresEnum.SnakeHeadRight);
        snakeSquares.put("0001H", SquaresEnum.SnakeHeadUp);
        snakeSquares.put("0010H", SquaresEnum.SnakeHeadDown);
        snakeSquares.put("0100H", SquaresEnum.SnakeHeadLeft);

        snakeSquares.put("1000T", SquaresEnum.SnakeTailRight);
        snakeSquares.put("0001T", SquaresEnum.SnakeTailUp);
        snakeSquares.put("0010T", SquaresEnum.SnakeTailDown);
        snakeSquares.put("0100T", SquaresEnum.SnakeTailLeft);

        snakeSquares.put("1001", SquaresEnum.SnakeRU);
        snakeSquares.put("1010", SquaresEnum.SnakeRD);
        snakeSquares.put("1100", SquaresEnum.SnakeRL);

        snakeSquares.put("0110", SquaresEnum.SnakeLD);
        snakeSquares.put("0101", SquaresEnum.SnakeLU);

        snakeSquares.put("0011", SquaresEnum.SnakeDU);


    }

    public static Image getSquareIcon(SquaresEnum square) {
        //System.out.println("getting: " + square.toString() + " Icon\n\n");
//        System.err.println(squaresIcons.get(square));
        Image image = new Image(squaresIcons.get(square));
        return image;
    }

    public static String getSquareIconPath(SquaresEnum square) {
//        System.err.println(squaresIcons.get(square));
        return squaresIcons.get(square);
    }

    public static SquaresEnum getSnakeSquareType(String connections) {
        return snakeSquares.get(connections);
    }

    /*
         snakeSquares.put(new boolean[]{true, false, false, false},SquaresEnum.SnakeRight);
        snakeSquares.put(new boolean[]{true, false, false, true},SquaresEnum.SnakeRU);
        snakeSquares.put(new boolean[]{true, false, true, false},SquaresEnum.SnakeRD);

        snakeSquares.put(new boolean[]{true, true, false, false},SquaresEnum.SnakeRL);
        snakeSquares.put(new boolean[]{false, true, true, false},SquaresEnum.SnakeLD);
        snakeSquares.put(new boolean[]{false, true, false, true},SquaresEnum.SnakeLU);
        snakeSquares.put(new boolean[]{false, true, false, false},SquaresEnum.SnakeLeft);

        snakeSquares.put(new boolean[]{false, false, true, false},SquaresEnum.SnakeLD);
        snakeSquares.put(new boolean[]{false, false, false, true},SquaresEnum.SnakeUp);
        snakeSquares.put(new boolean[]{false, false, true, false},SquaresEnum.SnakeDown);


     */
}
