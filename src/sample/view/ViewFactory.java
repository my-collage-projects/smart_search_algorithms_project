package sample.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import sample.controller.AbstractController;
import sample.controller.GameController;
import sample.controller.MainController;
import sample.controller.ModelAccess;

import javax.naming.OperationNotSupportedException;
import java.io.IOException;

public class ViewFactory {

    private ModelAccess modelAccess = new ModelAccess();

    private final static String MAIN_SCREEN_FXML = "MainLayout.fxml";
    private final static String GAME_SCENE_FXML = "GameLayout.fxml";

    public static ViewFactory defaultFactory = new ViewFactory();

    private static boolean mainViewInitialized = false;


    private Scene initializeScene(String fxmlPath , AbstractController controller){
        FXMLLoader loader;
        Parent parent;
        Scene scene;
        try {
            loader = new FXMLLoader(getClass().getResource(fxmlPath));
            loader.setController(controller);
            parent = loader.load();
        } catch (IOException e) {
//            System.out.println("IO Exception");
            return null;
        }
        scene = new Scene(parent);
        return scene;
    }

    public Scene getMainScene() throws OperationNotSupportedException {
        if (!mainViewInitialized) {
            MainController mainController = new MainController(modelAccess);
            ModelAccess.setMainController(mainController);

            mainViewInitialized = true;
            return initializeScene(MAIN_SCREEN_FXML, mainController);
        }
        else{
            throw new OperationNotSupportedException("Main Scene Already initialized");
        }
    }

    public Scene getGameScene(int size,int levelNumber) {
        GameController gameController = new GameController(modelAccess,size,levelNumber);
        return initializeScene(GAME_SCENE_FXML,gameController);
    }
}
