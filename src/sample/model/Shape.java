package sample.model;

import sample.MainApp;
import sample.controller.ModelAccess;
import sample.controller.ShapeFactory;
import sample.controller.SquareFactory;

import java.util.ArrayList;

public class Shape {
    boolean glass = true;
    ShapesEnum type;
    Square head;
    Square[] squares;
    private Square[][] squaresFrame;
    boolean flipped = false;
    private int indexInPuzzle;
    private int rotations = 0;
    private int flips = 0;

    public Shape(ShapesEnum type, int indexInPuzzle, boolean cloned) {
        this.indexInPuzzle = indexInPuzzle;
        this.type = type;
        if (!cloned) {
            initSquares(ModelAccess.getSquaresCount(type));
            squaresFrame = ShapeFactory.buildFrame(type, squares);
            ShapeFactory.buildConnections(squaresFrame);
        }
    }

    public Shape(Shape shape) {
        this.indexInPuzzle = shape.getIndexInPuzzle();
        this.type = shape.type;
        squares = new Square[shape.squares.length];
        for (int i = 0; i < shape.squares.length; i++) {
            if (shape.squares[i] != null) {
                if (shape.squares[i] instanceof SnakeSquare) {
                    squares[i] = new SnakeSquare(shape.squares[i]);
                } else if (shape.squares[i] instanceof StoneSquare) {
                    squares[i] = new StoneSquare(shape.squares[i]);
                } else if (shape.squares[i] instanceof GlassSquare) {
                    squares[i] = new GlassSquare(shape.squares[i]);
                }
            }
        }
        squaresFrame = ShapeFactory.buildFrame(type, squares);
    }

    private void initSquares(int num) {
        squares = new Square[num];
        for (int i = 0; i < squares.length; i++) {
//            System.out.println("Enter Square number: " + i);
            String input = MainApp.readInputLine();
            squares[i] = SquareFactory.buildSquare(input);
        }
    }

    @Override
    public String toString() {
        String result = "";
        switch (type) {
            case I: {
                result = "***\n" +
                        "***\n" +
                        "***\n" +
                        "***\n" +
                        "***\n" +
                        "***\n";
                break;
            }
            case L: {
                result = " ***\n" +
                        " ***\n" +
                        " ***\n" +
                        " ***\n" +
                        " *** ***\n" +
                        " *** ***\n";
                break;
            }
            case z: {
                result = "***\n" +
                        "***\n" +
                        "*** ***\n" +
                        "*** ***\n" +
                        "    ***\n" +
                        "    ***\n";
                break;
            }
            case sL: {
                result = "***\n" +
                        "***\n" +
                        "*** ***\n" +
                        "*** ***\n";
                break;
            }
            case sI: {
                result = "***\n" +
                        "***\n" +
                        "***\n" +
                        "***\n";
                break;
            }
        }
        return result;
    }


    public void rotateShape(String orientation) {
        if (orientation == "left") {
            rotateLeft();
            ShapeFactory.buildConnections(squaresFrame);
        }
        for (Square square : squares) {
            if (square != null) {
                if (square.isSnakeSquare()) {
                    SnakeSquare s = (SnakeSquare) square;
                    if (square.isFlipped()) {
                        s.rotateSnakeConnectionsLeft();
                    } else {
                        s.rotateSnakeConnectionsRight();
                    }
                }
            }
            //System.out.println(square.getType());
        }
        rotations++;
        rotations %= 4;
    }

    private void rotateLeft() {
        int N = squaresFrame.length;
        for (int x = 0; x < N / 2; x++) {
            // Consider elements in group
            // of 4 in current square
            for (int y = x; y < N - x - 1; y++) {
                // Store current cell in
                // temp variable
                Square temp = squaresFrame[x][y];

                // Move values from right to top
                squaresFrame[x][y] = squaresFrame[y][N - 1 - x];

                // Move values from bottom to right
                squaresFrame[y][N - 1 - x]
                        = squaresFrame[N - 1 - x][N - 1 - y];

                // Move values from left to bottom
                squaresFrame[N - 1 - x][N - 1 - y] = squaresFrame[N - 1 - y][x];

                // Assign temp to left
                squaresFrame[N - 1 - y][x] = temp;
            }
        }
    }

    public Square[][] getSquaresFrame() {
        return squaresFrame;
    }

    public void flipShape() {
        if (flipped) {
            flipped = false;
        } else {
            flipped = true;
        }
        for (Square square : squares) {
            square.flipSquare();
        }
        //squaresFrame = ShapeFactory.buildFrame(type, squares);
        flipFrame();
        ShapeFactory.buildConnections(squaresFrame);
    }

    private void flipFrame() {
        int n = squaresFrame.length;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n / 2; j++) {
                Square temp = squaresFrame[i][j];
                squaresFrame[i][j] = squaresFrame[i][n - j - 1];
                squaresFrame[i][n - j - 1] = temp;
            }
        }
        flips++;
        flips %= 2;
    }

/*    public void showHead() {
//        System.err.println(Arrays.toString(squares));
        squares[0].select();
    }

    public void removeSelection() {
        squares[0].cancelSelection();
    }*/

    public Square getHead() {
        return squares[0];
    }

    public void resetVisitedSquares() {
        for (Square square : squares) {
            square.setVisited(false);
        }
    }

    public void printConnections() {
        for (Square square : squares) {
            if (square instanceof SnakeSquare) {
                SnakeSquare snakeSquare = (SnakeSquare) square;
//                System.err.println(snakeSquare.getConnectionString() + "\n");
            }
        }
    }

    public void setSquaresFrame(Square[][] squaresFrame) {
        this.squaresFrame = squaresFrame;
        ShapeFactory.buildConnections(squaresFrame);
    }

/*
    public Square getSquareCopy(Square square) {
        if (square instanceof SnakeSquare) {
            SnakeSquare snakeSquare = (SnakeSquare) square;
            SnakeSquare copy = new SnakeSquare(square.getFrontType());
            copy.sDown = snakeSquare.sDown;
            copy.sLeft = snakeSquare.sLeft;
            copy.sRight = snakeSquare.sRight;
            copy.sUp = snakeSquare.sUp;
            copy.isSnakeTail = snakeSquare.isSnakeTail;
            copy.isSnakeHead = snakeSquare.isSnakeHead;
            return copy;
        } else if (square instanceof GlassSquare) {
            return new GlassSquare();
        }
//        else if(square instanceof StoneSquare){
        else {
            return new StoneSquare();
        }

    }
*/


/*    public Square[][] getCopyOfSquaresFrame() {
        Square[][] result = new Square[squaresFrame.length][squaresFrame.length];
        for (int i = 0; i < squaresFrame.length; i++) {
            for (int j = 0; j < squaresFrame.length; j++) {
                result[i][j] = getSquareCopy(squaresFrame[i][j]);
            }
        }
        return result;
    }*/


    public ArrayList<Integer> getPositionsOnMainGrid() {
        ArrayList<Integer> positions = new ArrayList<>();
        for (Square square : squares) {
            positions.add(square.getIndexI());
            positions.add(square.getIndexJ());
        }
        return positions;
    }

    public int getIndexInPuzzle() {
        return indexInPuzzle;
    }

    public Shape cloneShape() {
        Shape result = new Shape(type, indexInPuzzle, true);
        Square[][] cFrame = new Square[squaresFrame.length][squaresFrame.length];
        for (int i = 0; i < squaresFrame.length; i++) {
            for (int j = 0; j < squaresFrame.length; j++) {
                if (squaresFrame[i][j] != null) {
                    cFrame[i][j] = squaresFrame[i][j].cloneSquare();
                }
            }
        }
        Square[] cSquares = new Square[this.squares.length];
        for (int i = 0; i < cSquares.length; i++) {
            cSquares[i] = this.squares[i].cloneSquare();
        }

        result.setSquaresFrame(cFrame);
        result.setSquares(cSquares);
        return result;
    }

    private void setSquares(Square[] cSquares) {
        squares = cSquares;
    }

    public int getRotation() {
        return rotations;
    }

    public ShapesEnum getType() {
        return type;
    }
}