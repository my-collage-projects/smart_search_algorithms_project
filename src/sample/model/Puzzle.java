package sample.model;

import sample.MainApp;

import java.util.ArrayList;

public class Puzzle {

    ArrayList<Puzzle> nextStates = new ArrayList<>();
    private int weight;
    private int hurestic;
    Puzzle parent;
    int n, m;
    Square[][] squares;
    //TODO : 4 will be Dynamic
    Shape[] shapes;
    private boolean[] usedShapes;
    private boolean solved;
    private int stateId = -1;
    private SquaresEnum[][] types;
    private String lastMove;
    private int totalCost;

    public Puzzle(int n, int m) {
        this.n = n;
        this.m = m;
        squares = new Square[n][m];
        initWeight();
        totalCost = weight + hurestic;
    }


    public int getHurestic() {
        return hurestic;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setHurestic(int hurestic) {
        this.hurestic = hurestic;
    }

    public void setLastMove(String lastMove) {
        this.lastMove = lastMove;
    }

    public int getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(int totalCost) {
        this.totalCost = totalCost;
    }

    public void initWeight() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (squares[i][j] == null) {
                    weight++;
                }
                if (squares[i][j] instanceof SnakeSquare) {
                    hurestic++;
                }
            }
        }
        if(parent != null){
            weight += parent.getWeight();
            hurestic += parent.getHurestic();
        }
        initTotalCost();
    }

    public void initTotalCost(){
        totalCost = weight + hurestic;
    }

    public Puzzle(Puzzle state) {

        this.n = state.n;
        this.m = state.m;
        if (n == 0) {
            System.out.println("N!!! = 0");
        }
        squares = new Square[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (state.getSquare(i, j) == null) {
                    continue;
                }
                if (state.getSquare(i, j) instanceof SnakeSquare) {
                    squares[i][j] = new SnakeSquare(state.getSquare(i, j));
                } else if (state.getSquare(i, j) instanceof StoneSquare) {
                    squares[i][j] = new StoneSquare(state.getSquare(i, j));
                } else if (state.getSquare(i, j) instanceof GlassSquare) {
                    squares[i][j] = new GlassSquare(state.getSquare(i, j));
                }
            }
        }
        shapes = new Shape[state.getShapesCount()];
        usedShapes = state.cloneUsedShapes();
        for (int i = 0; i < state.getShapesCount(); i++) {
            shapes[i] = new Shape(state.shapes[i]);
        }

    }

    private Square getSquare(int i, int j) {
        return squares[i][j];
    }


    public SquaresEnum getSquareType(int i, int j) {
        if (squares[i][j] != null) {
            return squares[i][j].getType();
        }
        return SquaresEnum.Void;
    }


    public void initSquares() {
        //Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
//                System.out.print("Enter(" + i + ")(" + j + "): ");
//                String input = scanner.nextLine();
                String input = MainApp.readInputLine();
//                System.out.println(input);
//                squares[i][j] = SquareFactory.buildSquare(input);
                if (input.equals("sn")) {
//                    System.out.println("L/R/Up/Down");
//                    String type = scanner.nextLine();
                    String type = MainApp.readInputLine();
//                    System.out.println("============\n" + type);
                    squares[i][j] = new SnakeSquare(SquaresEnum.valueOf(type));
                } else if (input.equals("st")) {
                    squares[i][j] = new StoneSquare();
                } else if (input.equals("g")) {
                    squares[i][j] = new GlassSquare();
                } else if (input.equals("n")) {
                    squares[i][j] = null;
                } else {
                    j--;
                }
            }
        }

    }


    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (squares[i][j] != null) {
                    result += squares[i][j].getStringType();
                } else result += ": n :";
            }
            result += "\n";
        }

        for (int i = 0; i < shapes.length; i++) {
            if (shapes[i] != null) {
                result += shapes[i].toString() + "\n\n";
            }
        }

        return result;
    }

    public void initShapes() {
//        Scanner scanner = new Scanner(System.in);
        shapes = new Shape[Integer.parseInt(MainApp.readInputLine())];
        usedShapes = new boolean[shapes.length];
        for (int i = 0; i < shapes.length; i++) {
//            System.out.println("(" + i + ") L.sL.I.sI.z: ");
//            ShapesEnum type = ShapesEnum.valueOf(scanner.nextLine());
            ShapesEnum type = ShapesEnum.valueOf(MainApp.readInputLine());
            shapes[i] = new Shape(type, i, false);
        }
    }

    public Shape getShape(int i) {
        return shapes[i];
    }

    public int getShapesCount() {
        return shapes.length;
    }

    public SquaresEnum[][] getSquaresTypes() {
        return getTypes(squares);
    }

    public SquaresEnum[][] getShapeTypes(int i) {
        if (shapes[i] != null && !usedShapes[i]) {
            return getTypes(shapes[i].getSquaresFrame());
        }
        return null;
    }

    public SquaresEnum[][] getTypes(Square[][] squares) {
        SquaresEnum[][] types = null;
        try {
            types = new SquaresEnum[squares.length][squares[0].length];
        } catch (Exception e) {
            System.out.println(e);
        }
        for (int i = 0; i < squares.length; i++) {
            for (int j = 0; j < squares[0].length; j++) {
                if (squares[i][j] != null) {
                    types[i][j] = squares[i][j].getType();
                } else {
                    types[i][j] = SquaresEnum.Void;
                }
            }
        }
        return types;
    }

    public boolean canFit(int i, int j, Square head) {
        try {
            if (head.isVisited()) {
                return true;
            }
            head.setVisited(true);
            if (getSquareType(i, j) == SquaresEnum.Void) {
                if (head.isSnakeSquare() && !head.isFlipped()) {
                    SnakeSquare snakeSquare = (SnakeSquare) head;
//                    System.err.println("Checking connections for : " + i + j + head.getType());

                    if (!checkSnakeNeighbors(i, j, snakeSquare)) {
                        return false;
                    }
                } else {
                    if (!checkGlassNeighbors(i, j, head)) {
                        return false;
                    }
                }
                boolean l, r, d, u;
                r = l = d = u = true;
                if (head.hasLeft()) {
                    l = canFit(i, j - 1, head.left);
                }
                if (head.hasRight()) {
                    r = canFit(i, j + 1, head.right);
                }
                if (head.hasUp()) {
                    u = canFit(i - 1, j, head.up);
                }
                if (head.hasDown()) {
                    d = canFit(i + 1, j, head.down);
                }
                return l && r && d && u;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    private boolean checkGlassNeighbors(int i, int j, Square head) {
        if (j + 1 < squares.length && squares[i][j + 1] instanceof SnakeSquare && !squares[i][j + 1].isFlipped()) {
            if (((SnakeSquare) squares[i][j + 1]).hasSnakeLeft()) {
                return false;
            }
        }
        if (j - 1 >= 0 && squares[i][j - 1] instanceof SnakeSquare && !squares[i][j - 1].isFlipped()) {
            if (((SnakeSquare) squares[i][j - 1]).hasSnakeRight()) {
                return false;
            }
        }
        if (i + 1 < squares.length && squares[i + 1][j] instanceof SnakeSquare && !squares[i + 1][j].isFlipped()) {
            if (((SnakeSquare) squares[i + 1][j]).hasSnakeUp()) {
                return false;
            }
        }
        if (i - 1 >= 0 && squares[i - 1][j] instanceof SnakeSquare && !squares[i - 1][j].isFlipped()) {
            if (((SnakeSquare) squares[i - 1][j]).hasSnakeDown()) {
                return false;
            }
        }
        return true;

    }

    private boolean checkSnakeNeighbors(int i, int j, SnakeSquare head) {

//        System.err.println("\n---------------------------------\n" + head.getType() + "  has left:" + head.hasLeft());
        if (head.hasSnakeRight() && !head.hasRight()) {
            if (j + 1 >= squares.length || !(squares[i][j + 1] instanceof SnakeSquare) || !((SnakeSquare) squares[i][j + 1]).hasSnakeLeft()) {
                return false;
            }
        }
        if (head.hasSnakeLeft() && !head.hasLeft()) {
            if (j - 1 < 0 || !squares[i][j - 1].isSnakeSquare() || !((SnakeSquare) squares[i][j - 1]).hasSnakeRight()) {
                return false;
            }
        }
        if (head.hasSnakeUp() && !head.hasUp()) {
            if (i - 1 < 0 || !squares[i - 1][j].isSnakeSquare() || !((SnakeSquare) squares[i - 1][j]).hasSnakeDown()) {
                return false;
            }
        }
        if (head.hasSnakeDown() && !head.hasDown()) {
            if (i + 1 >= squares.length || !squares[i + 1][j].isSnakeSquare() || !((SnakeSquare) squares[i + 1][j]).hasSnakeUp()) {
                return false;
            }
        }
        return true;
    }


    public ArrayList<Integer> getAvailableMoves(Shape shape) {
        ArrayList<Integer> positions = new ArrayList<>();
        for (int i = 0; i < squares.length; i++) {
            for (int j = 0; j < squares.length; j++) {
                shape.resetVisitedSquares();
                if (canFit(i, j, shape.getHead())) {
                    positions.add(i * 10 + j);
                }
            }
        }
        return positions;
    }

    public void putShapeInPosition(int i, int j, Square head) {
        if (head.isVisited()) {
            return;
        }
        head.setVisited(true);
//        System.err.println("i-" + i + "   j-" + j);
        squares[i][j] = head;
        head.setPosition(i, j);
        if (head.hasLeft()) {
            putShapeInPosition(i, j - 1, head.left);
        }
        if (head.hasRight()) {
            putShapeInPosition(i, j + 1, head.right);
        }
        if (head.hasDown()) {
            putShapeInPosition(i + 1, j, head.down);
        }
        if (head.hasUp()) {
            putShapeInPosition(i - 1, j, head.up);
        }
    }

    public void removeShape(int index) {
//        System.err.println("Removed Shape Index : " + index);
        shapes[index] = null;
    }

 /*   public ArrayList<Shape> getAvailableShapes() {
        ArrayList<Shape> result = new ArrayList<>();
        for (int i = 0; i < shapes.length; i++) {
            if (shapes[i] != null) {
                result.add(shapes[i]);
            }
        }
        return result;
    }*/

    public ArrayList<Integer> getAvailableMovesList(Shape shape) {
        ArrayList<Integer> positions = new ArrayList<>();
        for (int i = 0; i < squares.length; i++) {
            for (int j = 0; j < squares.length; j++) {
                shape.resetVisitedSquares();
                if (canFit(i, j, shape.getHead())) {
                    positions.add(i);
                    positions.add(j);
                }
            }
        }
        return positions;
    }

    public int getN() {
        return n;
    }

    public int getM() {
        return m;
    }

    public Square[][] getSquares() {
        return squares;
    }

    public void setSquares(Square[][] squares) {
        this.squares = squares;
    }

/*
    public void setShapes(ArrayList<Shape> availableShapes) {
        shapes = new Shape[availableShapes.size()];
        int i = 0;
        for (Shape availableShape : availableShapes) {
            shapes[i] = availableShape;
            i++;
        }
    }
*/

    public Shape[] cloneShapes() {
        Shape[] result = new Shape[shapes.length];
        for (int i = 0; i < shapes.length; i++) {
            if (usedShapes[i]) {
                continue;
            }
            result[i] = shapes[i].cloneShape();
        }
        return result;
    }

    public void setShapes(Shape[] shapes) {
        this.shapes = shapes;
    }

    public ArrayList<Puzzle> getNextStates() {
        return nextStates;
    }

    public boolean checkSolved() {
        for (Square[] square : squares) {
            for (Square square1 : square) {
                if (square1 == null) {
                    return false;
                }
            }
        }
        return true;
    }

    public ArrayList<Integer> getAvailableShapesIndices() {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < shapes.length; i++) {
            if (shapes[i] != null) {
                result.add(i);
            }
        }
        return result;
    }

    public ArrayList<Shape> getAvailableShapes() {
        ArrayList<Shape> result = new ArrayList<>();
        for (int i = 0; i < shapes.length; i++) {
            if (!usedShapes[i]) {
                result.add(shapes[i]);
            }
        }
        return result;
    }

    public void putShapeBack(Shape shape) {
        ArrayList<Integer> position = shape.getPositionsOnMainGrid();
        for (int i = 0; i < position.size(); i += 2) {
            squares[position.get(i)][position.get(i + 1)] = null;
        }

    }

    public void setUsedShape(int indexInPuzzle, boolean b) {
        usedShapes[indexInPuzzle] = b;
    }

    public boolean[] getUsedShapes() {
        return usedShapes;
    }

    public void setUsedShapes(boolean[] usedShapes) {
        this.usedShapes = usedShapes;
    }

    public void copySquares(Square[][] squares) {
        for (int i = 0; i < squares.length; i++) {
            for (int j = 0; j < squares.length; j++) {
                this.squares[i][j] = squares[i][j];
            }
        }
    }

    public Shape[] getShapes() {
        return shapes;
    }

    public boolean[] cloneUsedShapes() {
        return usedShapes.clone();
    }

    public void setSolved(boolean b) {
        solved = b;
    }

    public boolean isSolved() {
        return solved;
    }

    public void setStateId(int id) {
        this.stateId = id;
    }

    public int getStateId() {
        return stateId;
    }

    public void saveTypes() {
        types = getTypes(squares);
    }

    public void loadTypes() {
        for (int i = 0; i < squares.length; i++) {
            for (int j = 0; j < squares.length; j++) {
                if (squares[i][j] == null) {
                    continue;
                }
                if (squares[i][j].getType() != types[i][j]) {
                    squares[i][j].setType(types[i][j]);
                }
            }
        }
    }

    public void setParent(Puzzle currentState) {
        parent = currentState;
    }

    public Puzzle getParent() {
        return parent;
    }

    public String getLastMove() {
        return lastMove;
    }

    public void setLastMove(ShapesEnum type, int rotation) {
        lastMove = "Shape Type: " + type + "  rotations: " + rotation;
    }

    public void addNextState(Puzzle nextState) {
        nextStates.add(nextState);
    }

    public boolean equals(Puzzle puzzle) {

        if (puzzle.getSquaresTypes() == this.getSquaresTypes()) {
            if (puzzle.getUsedShapes() == usedShapes) {
                return true;
            }
        }
        return false;
    }

    public int getUsedShapeCount() {
        int result = 0;
        for (boolean usedShape : usedShapes) {
            if (usedShape) {
                result++;
            }
        }
        return result;
    }

    public void setNextStates(ArrayList<Puzzle> allNextStates) {
        nextStates = allNextStates;
    }


/*
    private void resetHighlights() {
        for (Square[] square : squares) {
            for (Square square1 : square) {
                if (square1 != null) {
                    square1.unhighlight();
                }
            }
        }
    }


    private void highlightSquare(int i, int j) {
        if (squares[i][j] != null) {
            squares[i][j].highlightImage();
        }
    }

    public void setSquareImage(int i, int j, ImageView imageView) {
        if (squares[i][j] != null) {
            squares[i][j].setImage(imageView);
        }
    }*/
}
