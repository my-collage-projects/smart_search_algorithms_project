package sample.model;

public enum SquaresEnum {
    SnakeHeadRight,
    SnakeHeadLeft,
    SnakeHeadDown,
    SnakeHeadUp,
    SnakeTailRight,
    SnakeTailLeft,
    SnakeTailDown,
    SnakeTailUp,
    SnakeRL,
    SnakeRD,
    SnakeRU,
    SnakeLD,
    SnakeLU,
    SnakeDU,
    Stone,
    Glass,
    Void,
    Head
}
