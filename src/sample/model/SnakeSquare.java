package sample.model;

import sample.view.StyleConstants;

public class SnakeSquare extends Square {

    boolean sLeft, sRight, sUp, sDown;
    boolean isSnakeTail;
    boolean isSnakeHead;

    public SnakeSquare(SquaresEnum squareType) {
        frontType = squareType;
        type = frontType;
        if (type.toString().contains("Head")) {
            isSnakeHead = true;
        } else if (type.toString().contains("Tail")) {
            isSnakeTail = true;
        }
        makeConnections();
    }

    public SnakeSquare(Square square){
        SnakeSquare snakeSquare = (SnakeSquare)square;
        frontType = square.getFrontType();
        type = square.getType();
        isSnakeHead = snakeSquare.isSnakeHead;
        isSnakeTail = snakeSquare.isSnakeTail;
        setFlipped(snakeSquare.isFlipped());
        sLeft = snakeSquare.sLeft;
        sRight = snakeSquare.sRight;
        sUp = snakeSquare.sUp;
        sDown = snakeSquare.sDown;
    }

    private void makeConnections() {
        sRight = sLeft = sUp = sDown = false;
        if (type == SquaresEnum.SnakeHeadRight ||
                type == SquaresEnum.SnakeTailRight ||
                type == SquaresEnum.SnakeRD ||
                type == SquaresEnum.SnakeRL ||
                type == SquaresEnum.SnakeRU) {
            sRight = true;
        }
        if (type == SquaresEnum.SnakeHeadLeft ||
                type == SquaresEnum.SnakeTailLeft ||
                type == SquaresEnum.SnakeLD ||
                type == SquaresEnum.SnakeLU ||
                type == SquaresEnum.SnakeRL) {
            sLeft = true;
        }
        if (type == SquaresEnum.SnakeHeadDown ||
                type == SquaresEnum.SnakeTailDown ||
                type == SquaresEnum.SnakeDU ||
                type == SquaresEnum.SnakeLD ||
                type == SquaresEnum.SnakeRD) {
            sDown = true;
        }
        if (type == SquaresEnum.SnakeHeadUp ||
                type == SquaresEnum.SnakeTailUp ||
                type == SquaresEnum.SnakeDU ||
                type == SquaresEnum.SnakeLU ||
                type == SquaresEnum.SnakeRU) {
            sUp = true;
        }
    }


    @Override
    public String getStringType() {
        if (isFlipped()) {
            return ": G";
        }
        String result = ": Sn(";
        if (sRight) {
            result += "r";
        }
        if (sLeft) {
            result += "l";
        }
        if (sUp) {
            result += "u";
        }
        if (sDown) {
            result += "d";
        }
        result += ")";
        return result;
    }

    public boolean hasSnakeLeft() {
        return sLeft;
    }

    public boolean hasSnakeRight() {
        return sRight;
    }

    public void rotateSnakeConnectionsRight() {

        boolean temp = sRight;
        sRight = sDown;
        sDown = sLeft;
        sLeft = sUp;
        sUp = temp;

        updateType();

    }

    public void rotateSnakeConnectionsLeft() {
        boolean temp = sRight;
        sRight = sUp;
        sUp = sLeft;
        sLeft = sDown;
        sDown = temp;

        updateType();
        /**
         *  important difference
         */
        useBackFace();
    }


    private void updateType() {
        int i = getIndexI();
        int j = getIndexJ();
        String str = getConnectionString();
        frontType = StyleConstants.getSnakeSquareType(str);

        if (frontType == null) {
            System.out.println("front null " + i + j);

        }
        type = frontType;
    }

    public String getConnectionString() {
        boolean[] connections = {sRight, sLeft, sDown, sUp};
        String result = "";
        for (boolean connection : connections) {
            if (connection) result += "1";
            else result += "0";
        }
        if (isSnakeHead()) {
            result += "H";
        } else if (isSnakeTail()) {
            result += "T";
        }
        return result;
    }

    private boolean isSnakeHead() {
        return isSnakeHead;
    }

    private boolean isSnakeTail() {
        return isSnakeTail;
    }

    public boolean hasSnakeDown() {
        return sDown;
    }

    public boolean hasSnakeUp() {
        return sUp;
    }

    @Override
    public Square cloneSquare() {
        SnakeSquare result = new SnakeSquare(frontType);
        result.isSnakeHead = isSnakeHead;
        result.isSnakeTail = isSnakeTail;
        result.setSnakeConnections(sRight, sLeft, sDown, sUp);
        result.setFlipped(super.isFlipped());
        result.setConnections(right, left, down, up);
        return result;
    }

    private void setSnakeConnections(boolean sRight, boolean sLeft, boolean sDown, boolean sUp) {
        this.sRight = sRight;
        this.sLeft = sLeft;
        this.sDown = sDown;
        this.sUp = sUp;
    }

    @Override
    public void setType(SquaresEnum squaresEnum) {
        //super.setType(squaresEnum);
        if (squaresEnum == SquaresEnum.Glass) {
            if (!isFlipped()) {
                flipSquare();
            }
        } else {
            frontType = squaresEnum;
            if (frontType == null) {
                int i;
            }
            type = frontType;
            if (isFlipped()) {
                flipSquare();
            }

            makeConnections();
        }
    }
}
