package sample.model;

public class StoneSquare extends Square {

    public StoneSquare() {
        frontType = SquaresEnum.Stone;
        type = frontType;
    }

    public StoneSquare(Square square) {
        frontType = square.frontType;
        type = frontType;
        setFlipped(square.isFlipped());
    }

    @Override
    public String getStringType() {
        return ": St :";
    }

    @Override
    public Square cloneSquare() {
        return new StoneSquare();
    }
}
