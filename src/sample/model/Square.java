package sample.model;

public abstract class Square {


    Square left, right, up, down;
    SquaresEnum type;
    SquaresEnum frontType;
    SquaresEnum backType = SquaresEnum.Glass;
    private boolean flipped;
//    private boolean selected;

    private boolean visited;
    private int indexI = -1;
    private int indexJ = -1;

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public boolean isVisited() {
        return visited;
    }

    public abstract String getStringType();

    public SquaresEnum getType() {
//        if (selected) {
//            return SquaresEnum.Head;
//        }
        return type;
    }

    public Square cloneSquare(){
        return null;
    }

    public boolean isSnakeSquare() {
        if(frontType == null){
            System.out.println(type + " NULL |||||||||||||||||||" );
            System.out.println(type);
        }
        return frontType.toString().contains("Snake");
    }

    public void useFrontFace() {
        type = frontType;
    }

    public boolean isFlipped() {
        return flipped;
    }

    public void useBackFace() {
        type = backType;
    }

    public void flipSquare() {
        if (flipped) {
            flipped = false;
            useFrontFace();
        } else {
            flipped = true;
            useBackFace();
        }
    }


    public void setLeft(Square left) {
        this.left = left;
    }

    public void setRight(Square right) {
        this.right = right;
    }

    public void setUp(Square up) {
        this.up = up;
    }

    public void setDown(Square down) {
        this.down = down;
    }


 /*   public void cancelSelection() {
        selected = false;
    }

    public void select() {
        selected = true;
    }*/

    public boolean hasLeft() {
        return left != null;
    }

    public boolean hasRight() {
        return right != null;
    }

    public boolean hasUp() {
        return up != null;
    }

    public boolean hasDown() {
        return down != null;
    }


    public void resetNeighbors() {
        left = right = down = up = null;
    }

    public SquaresEnum getFrontType() {
        return frontType;
    }

    public void setPosition(int i, int j) {
        this.indexI = i;
        this.indexJ = j;
    }



 /*   public void setImage(ImageView imageView) {
        this.imageView = imageView;
    }

    public void highlightImage() {
        if (!highlighted) {
            imageView.setFitHeight(imageView.getFitHeight() + 5);
            imageView.setFitWidth(imageView.getFitWidth() + 5);
            highlighted = true;
        }
    }

    public void unhighlight() {
        if(highlighted) {
            imageView.setFitHeight(imageView.getFitHeight() - 5);
            imageView.setFitWidth(imageView.getFitWidth() - 5);
            highlighted = false;
        }
    }*/

    public int getIndexI() {
        return indexI;
    }

    public int getIndexJ() {
        return indexJ;
    }

    protected void setFlipped(boolean flipped) {
        this.flipped = flipped;
    }

    protected void setConnections(Square right, Square left, Square down, Square up) {
        this.right = right;
        this.left = left;
        this.down = down;
        this.up = up;
    }

    public void setType(SquaresEnum squaresEnum) {
        type = squaresEnum;
    }
}
