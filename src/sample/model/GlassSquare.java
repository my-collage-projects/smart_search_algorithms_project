package sample.model;

public class GlassSquare extends Square{

    public GlassSquare() {
        frontType = SquaresEnum.Glass;
        type = frontType;
    }

    public GlassSquare(Square square) {
        frontType = square.frontType;
        type = frontType;
    }

    @Override
    public String getStringType() {
        return ": G :";
    }

    @Override
    public Square cloneSquare() {
        return new GlassSquare();
    }
}
