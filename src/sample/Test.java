package sample;

import sample.model.Puzzle;

public class Test {
    public static Puzzle puzzle;
    public static void main(String[] args) {
        puzzle = new Puzzle(3,3);
        puzzle.initSquares();
        puzzle.initShapes();
        System.out.println(puzzle);
    }
    //#03a9f4
}
