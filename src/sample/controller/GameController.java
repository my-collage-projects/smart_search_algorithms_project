package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import sample.MainApp;
import sample.model.Puzzle;
import sample.model.Shape;
import sample.model.SquaresEnum;
import sample.view.StyleConstants;

import java.net.URL;
import java.util.*;

public class GameController extends AbstractController implements Initializable {

    Puzzle puzzle;
    @FXML
    GridPane grid;

    @FXML
    VBox sideBarRight;

    @FXML
    Button btnRotate;

    @FXML
    Button btnFlip;

    @FXML
    Button btnRefresh;
    @FXML
    Button btnDFS;

    @FXML
    Button btnBFS;

    @FXML
    Button btnUCS;

    @FXML
    Button btnA_Star;

    @FXML
    Button btnNewGame;

    @FXML
    VBox sideBarLeft;
    private ArrayList<Puzzle> statesList = new ArrayList<>();


    private PriorityQueue<Puzzle> UCS_Queue = new PriorityQueue<>(Comparator.comparing(Puzzle::getWeight));
    private PriorityQueue<Puzzle> AStar_Queue = new PriorityQueue<>(Comparator.comparing(Puzzle::getTotalCost));

    private int id = 0;

    private int[] statePerLevel;


    @FXML
    void actionHandler(ActionEvent event) {
        if (event.getSource().equals(btnRotate)) {
            rotateSelectedShape();
        }
        if (event.getSource().equals(btnFlip)) {
            flipSelectedShape();
        }
        if (event.getSource().equals(btnRefresh)) {
            updateSidebar();
        }
        if (event.getSource().equals(btnDFS)) {
            puzzle.setStateId(++id);
            puzzle.saveTypes();
            statePerLevel = new int[puzzle.getShapesCount() + 1];
            long startTime = System.currentTimeMillis();
            solveDFS(puzzle);
            long timePassed = System.currentTimeMillis() - startTime;
            System.out.println("Time : " + timePassed + "ms");

        }
        if (event.getSource().equals(btnBFS)) {
            statesList.add(puzzle);
            puzzle.setStateId(++id);
            puzzle.saveTypes();
            initStatePerLevel();

            long startTime = System.currentTimeMillis();
            solveBFS(puzzle);
            long timePassed = System.currentTimeMillis() - startTime;
            System.out.println("Time : " + timePassed + "ms");
        }
        if (event.getSource().equals(btnUCS)) {
            UCS_Queue.add(puzzle);
            puzzle.setStateId(++id);
            puzzle.saveTypes();
            initStatePerLevel();
            long startTime = System.currentTimeMillis();
            solveUCS();
            long timePassed = System.currentTimeMillis() - startTime;
            System.out.println("Time : " + timePassed + "ms");

        }
        if (event.getSource().equals(btnA_Star)) {
            AStar_Queue.add(puzzle);
            puzzle.setStateId(++id);
            puzzle.saveTypes();
            initStatePerLevel();
            long startTime = System.currentTimeMillis();
            solveA_Star();
            long timePassed = System.currentTimeMillis() - startTime;
            System.out.println("Time : " + timePassed + "ms");
        }

        if(event.getSource().equals(btnNewGame)){
            startNewGame();
        }
    }

    private void initStatePerLevel() {
        statePerLevel = new int[puzzle.getShapesCount() + 1];
    }

    int size;
    int levelNumber;

    public GameController(ModelAccess modelAccess, int size, int levelNumber) {
        super(modelAccess);
        this.size = size;
        this.levelNumber = levelNumber;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        startNewGame();
    }

    private void startNewGame() {
        id = 0;
        AStar_Queue.clear();
        UCS_Queue.clear();
        statesList.clear();

        MainApp.loadLevel(levelNumber);
        puzzle = new Puzzle(size, size);
        puzzle.initSquares();
        System.err.println("Squares has been Initialized");

        initMainGrid(puzzle.getSquaresTypes(), 75);
        System.err.println("main grid has been initialized");

        puzzle.initShapes();
        System.err.println("shapes has been initialized");

        initSideBar();
        System.err.println("side bar has been initialized");

        //solve_using_dfs(puzzle);
        // solveDFS(puzzle);
    }


    private boolean solveA_Star() {
        return solve(AStar_Queue);
    }

    private boolean solveUCS() {
        return solve(UCS_Queue);
    }


    private boolean solve(PriorityQueue priorityQueue) {
        Puzzle currentState = (Puzzle) priorityQueue.poll();
        currentState.loadTypes();
        if (currentState.checkSolved()) {
            System.out.println("Solved, State's id : " + currentState.getStateId());
            puzzle = currentState;
            puzzle.setSolved(true);
            showSolution(currentState);
            initializeGUI(currentState);
            return true;
        }
        priorityQueue.addAll(getAllNextStates(currentState));
        System.out.println("STATE ID : " + currentState.getStateId() + "\nSIZE : " + priorityQueue.size());
        System.out.println(statePerLevel[0] + " | " + statePerLevel[1] + " | " + statePerLevel[2] + " | " + statePerLevel[3] + " | " + statePerLevel[4]);
        if (priorityQueue.isEmpty()) {
            System.err.println("Empty states list");
            return false;
        }
        return solve(priorityQueue);


    }

    /******************************************************************************
     *                          Solve with #BFS
     * ****************************************************************************
     * ****************************************************************************
     */
    private boolean solveBFS(Puzzle currentState) {
        statesList.remove(0);
        currentState.loadTypes();
        if (currentState.checkSolved()) {
            System.out.println("Solved, State's id : " + currentState.getStateId());
            puzzle = currentState;
            puzzle.setSolved(true);
            showSolution(currentState);
            initializeGUI(currentState);
            return true;
        }
        statesList.addAll(getAllNextStates(currentState));
        System.out.println("STATE ID : " + currentState.getStateId() + "\nSIZE : " + statesList.size());
        System.out.println(statePerLevel[0] + " | " + statePerLevel[1] + " | " + statePerLevel[2] + " | " + statePerLevel[3] + " | " + statePerLevel[4]);
        if (statesList.isEmpty()) {
            System.err.println("Empty states list");
            return false;
        }
        return solveBFS(statesList.get(0));
    }

    private void showSolution(Puzzle currentState) {
        if (currentState == null) {
            return;
        }
        System.out.println(currentState.getLastMove());
        showSolution(currentState.getParent());
    }

    private ArrayList<Puzzle> getAllNextStates(Puzzle currentState) {
//        System.err.println("Assigning states to state(" + currentState.getStateId() + ")...");
        ArrayList<Shape> availableShapes = currentState.getAvailableShapes();
        ArrayList<Puzzle> result = new ArrayList<>();
        availableShapes.forEach(shape -> {
            /** for every Shape we do the next :*/
            //System.err.println("start with shape index(" + shape.getIndexInPuzzle() + ")");
            result.addAll(checkAllRotations(currentState, shape));
            shape.flipShape();
            result.addAll(checkAllRotations(currentState, shape));
        });
        return result;
    }

    private ArrayList<Puzzle> checkAllRotations(Puzzle currentState, Shape shape) {
//        System.err.println("Checking all rotations for Shape Index :" + shape.getIndexInPuzzle());
        ArrayList<Puzzle> result = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            result.addAll(getNextStatesForShape(currentState, shape));
            shape.rotateShape("left");
        }
        return result;
    }

    private ArrayList<Puzzle> getNextStatesForShape(Puzzle currentState, Shape shape) {
//        System.out.println("adding states to puzzle for shape index : " + shape.getIndexInPuzzle());
        ArrayList<Integer> moves = currentState.getAvailableMovesList(shape);
//        System.err.println("Available moves:\n" + "=========================================" + moves.toString());
        ArrayList<Puzzle> nextStates = new ArrayList<>();
        for (int i = 0; i < moves.size(); i += 2) {

            //TODO : COPYING METHOD
            /*
            Puzzle nextState = new Puzzle(currentState);
            nextState.putShapeInPosition(moves.get(i), moves.get(i + 1), shape.getHead());
            nextState.setUsedShape(shape.getIndexInPuzzle(), true);
            if(isVisited(nextState)){
                continue;
            }

            bfsLevel[nextState.getUsedShapeCount()]++;


            nextState.setParent(currentState);
//            System.out.println(nextState.toString());
            nextState.setStateId(++id);
//            System.out.println("next State ID: " + nextState.getStateId());
            statesList.add(nextState);*/

            Puzzle nextState = new Puzzle(currentState.getN(), currentState.getM());

            nextState.setStateId(++id);
            nextState.copySquares(currentState.getSquares());

            nextState.setShapes(currentState.getShapes());
            nextState.setUsedShapes(currentState.cloneUsedShapes());

            shape.resetVisitedSquares();
            nextState.putShapeInPosition(moves.get(i), moves.get(i + 1), shape.getHead());
            nextState.setUsedShape(shape.getIndexInPuzzle(), true);
            nextState.setParent(currentState);

            statePerLevel[nextState.getUsedShapeCount()]++;

            nextState.setLastMove(shape.getType(), shape.getRotation());
            //TODO : This is the idea of types
            nextState.saveTypes();
            nextStates.add(nextState);

/**
 *      Method 2:
 */
            /*
            try {
                Puzzle nextState = (Puzzle) puzzle.clone();
                shape.resetVisitedSquares();
                nextState.putShapeInPosition(moves.get(i), moves.get(i + 1), puzzle.getShape(i).getHead());
                nextState.setUsedShape(shape.getIndexInPuzzle(), true);
                statesList.add(nextState);
            } catch (CloneNotSupportedException | ClassCastException e) {
                e.printStackTrace();
            }
*/
        }
        return nextStates;
    }

    private boolean isVisited(Puzzle nextState) {
        for (Puzzle puzzle1 : statesList) {
            if (nextState.equals(puzzle1)) {
                System.out.println("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV");
                return true;
            }
        }
        return false;
    }

    /******************************************************************************
     * ****************************************************************************
     * ****************************************************************************
     */


    public void initializeGUI(Puzzle currentState) {
        initMainGrid(currentState.getSquaresTypes(), 75);
        System.err.println("main grid has been initialized");

        initSideBar();
        System.err.println("side bar has been initialized");
    }


    /*   private Puzzle copyState(Puzzle puzzle) {
           System.err.println("Copying current State");
           Puzzle newState = new Puzzle(puzzle.getN(), puzzle.getM());
           Square[][] squares = puzzle.getSquares().clone();
           Shape shapes[] = new Shape[puzzle.getAvailableShapes().size()];
           int i =0;
           for (Shape shape : puzzle.getAvailableShapes()){

               shapes[i].setSquares(shape.getSquareCopy());
               shapes[i].setSquaresFrame(shape.getCopyOfSquaresFrame());
           }
           newState.setSquares(squares);
           newState.setShapes(puzzle.getAvailableShapes());
           return newState;
       }*/
    private void initSideBar() {
//        System.out.println("Initializing side bar : \n\n\n");
        sideBarRight.getChildren().clear();
        sideBarLeft.getChildren().clear();
        VBox sideBar = sideBarRight;
        for (int i = 0; i < puzzle.getShapesCount(); i++) {
            if (i > puzzle.getShapesCount() / 2 - 1) {
                sideBar = sideBarLeft;
            }
            SquaresEnum[][] types = puzzle.getShapeTypes(i);
            if (types == null) {
                continue;
            }
            GridPane shape = new GridPane();
            getModelAccess().addShape(shape);
            int finalI = i;
            shape.setOnMouseClicked(event -> {
                if (getModelAccess().getSelectedShape() != null) {
                    if (getModelAccess().getSelectedShapeGrid() != shape) {
//                        getModelAccess().getSelectedShape().removeSelection();
                        updateSidebar();
                    } else {
                        event.consume();
                    }
                }
                //TODO :  check these next lines
                getModelAccess().setSelectedShapeIndex(finalI);
                getModelAccess().setSelectedShape(puzzle.getShape(finalI));
                getModelAccess().setSelectedShapeGrid(shape);
//                puzzle.getShape(finalI).showHead();
                updateSidebar();
                getModelAccess().getSelectedShape().printConnections();
                getModelAccess().resetHighlightedSquares();
                getModelAccess().highlightSquares(puzzle.getAvailableMoves(puzzle.getShape(finalI)));
            });
            initShapesGrid(shape, types, 40);

            sideBar.getChildren().add(shape);
        }
    }

    public void initMainGrid(SquaresEnum[][] types, int size) {
        getModelAccess().resetMainGridImages();
        for (int i = 0; i < types.length; i++) {
            for (int j = 0; j < types.length; j++) {

                ImageView imageView = new ImageView(new Image(StyleConstants.getSquareIconPath(types[i][j])));

                getModelAccess().addMainGridImage(i, j, imageView);

                imageView.setFitWidth(size);
                imageView.setFitHeight(size);
                int finalJ = j;
                int finalI = i;
                imageView.setOnMouseClicked(event -> {
                    getModelAccess().getSelectedShape().resetVisitedSquares();
                    boolean bool = puzzle.canFit(finalI, finalJ, getModelAccess().getSelectedShape().getHead());
                    //System.out.print(bool);
                    if (bool) {
                        getModelAccess().getSelectedShape().resetVisitedSquares();
                        getModelAccess().resetHighlightedSquares();
                        puzzle.putShapeInPosition(finalI, finalJ, getModelAccess().getSelectedShape().getHead());
                        puzzle.setUsedShape(getModelAccess().getSelectedShape().getIndexInPuzzle(), true);

                        //TODO : remove shape from sidebar
                        initMainGrid(puzzle.getSquaresTypes(), 75);
                        //puzzle.removeShape(getModelAccess().getSelectedShapeIndex());
                        initSideBar();
                        getModelAccess().setSelectedShape(null);
                    }
                });
                grid.add(imageView, j, i);
            }
        }
    }


    public void initShapesGrid(GridPane grid, SquaresEnum[][] types, int size) {
        grid.getChildren().clear();
        for (int i = 0; i < types.length; i++) {
            for (int j = 0; j < types.length; j++) {
                // System.out.println(types[i][j] + "--");
                ImageView imageView = new ImageView(StyleConstants.getSquareIcon(types[i][j]));
                imageView.setFitWidth(size);
                imageView.setFitHeight(size);
                int finalJ1 = j;
                int finalI1 = i;
                imageView.setOnMouseClicked(event -> {
//                    System.err.println(types[finalI1][finalJ1].toString());
                });
                grid.add(imageView, j, i);
            }
//            System.out.println("\n");
        }
    }

    public void updateSidebar() {
        initShapesGrid(
                getModelAccess().getSelectedShapeGrid(),
                puzzle.getTypes(getModelAccess().getSelectedShape().getSquaresFrame()),
                40);
    }


    private void flipSelectedShape() {
        getModelAccess().getSelectedShape().flipShape();
        updateSidebar();

    }

    private void rotateSelectedShape() {
        getModelAccess().getSelectedShape().rotateShape("left");
        updateSidebar();
    }


    /******************************************************************************
     *                          Solve with #DFS
     * ****************************************************************************
     * ****************************************************************************
     */
    public boolean solveDFS(Puzzle currentState) {
        currentState.setStateId(id++);
        currentState.loadTypes();
        if (currentState.checkSolved()) {
            System.out.println("Solved, State's id : " + currentState.getStateId());

            puzzle = currentState;
            puzzle.setSolved(true);
            showSolution(currentState);
            initializeGUI(currentState);
            System.out.println(currentState.getStateId());
            return true;
        }
        ArrayList<Puzzle> nextStates = getAllNextStates(currentState);
        System.out.println("STATE ID : " + currentState.getStateId() + "\nSIZE : " + statesList.size());
        System.out.println(statePerLevel[0] + " | " + statePerLevel[1] + " | " + statePerLevel[2] + " | " + statePerLevel[3] + " | " + statePerLevel[4]);

        if (nextStates.isEmpty()) {
            return false;
        }
        for (Puzzle nextState : nextStates) {
            if (solveDFS(nextState)) {
                return true;
            }
        }
        System.out.println("No solution");
        /*
        ArrayList<Shape> availableShapes = currentState.getAvailableShapes();
        for (Shape shape : availableShapes) {
            if (checkRotations(currentState, shape)) {
                return true;
            }
            shape.flipShape();
            if (checkRotations(currentState, shape)) {
                return true;
            }
        }
*/

        return false;
    }

    private boolean checkRotations(Puzzle currentState, Shape shape) {
        for (int j = 0; j < 4; j++) {
            ArrayList<Integer> availableMoves = currentState.getAvailableMovesList(shape);
            for (int i = 0; i < availableMoves.size(); i += 2) {
                System.err.println("*");
                shape.resetVisitedSquares();
                currentState.putShapeInPosition(availableMoves.get(i), availableMoves.get(i + 1), shape.getHead());

                currentState.setUsedShape(shape.getIndexInPuzzle(), true);
                if (solveDFS(currentState)) {
                    return true;
                }
                currentState.setUsedShape(shape.getIndexInPuzzle(), false);
                currentState.putShapeBack(shape);
            }
            shape.rotateShape("left");
        }
        return false;
    }

    /******************************************************************************
     * ****************************************************************************
     * ****************************************************************************
     */

}
