package sample.controller;

import sample.model.ShapesEnum;
import sample.model.Square;

public class ShapeFactory {


    public static Square[][] buildFrame(ShapesEnum shape, Square[] squares) {
        //TODO : this should be dynamic size
        Square[][] frame = new Square[3][3];
        try {
            if (shape == ShapesEnum.L) {
                frame[0][0] = squares[0];
                frame[1][0] = squares[1];
                frame[2][0] = squares[2];
                frame[2][1] = squares[3];
            } else if (shape == ShapesEnum.sL) {
                frame[0][0] = squares[0];
                frame[1][0] = squares[1];
                frame[1][1] = squares[2];
            } else if (shape == ShapesEnum.I) {
                frame[0][0] = squares[0];
                frame[1][0] = squares[1];
                frame[2][0] = squares[2];
            } else if (shape == ShapesEnum.sI) {
                frame[0][0] = squares[0];
                frame[1][0] = squares[1];
            }
        } catch (Exception e) {
//            System.out.println(e);
            return null;
        }
        return frame;
    }

    public static void buildConnections(Square[][] squaresFrame) {
        /**
         * @squareFrame should be a quadratic matrix (n=m)
         */
        int n = squaresFrame.length;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (squaresFrame[i][j] != null) {
                    squaresFrame[i][j].resetNeighbors();
                    if (hasLeft(squaresFrame, i, j)) {
                        squaresFrame[i][j].setLeft(squaresFrame[i][j - 1]);
                    }
                    if (hasRight(squaresFrame, i, j)) {
                        squaresFrame[i][j].setRight(squaresFrame[i][j + 1]);
                    }
                    if (hasUp(squaresFrame, i, j)) {
                        squaresFrame[i][j].setUp(squaresFrame[i - 1][j]);
                    }
                    if (hasDown(squaresFrame, i, j)) {
                        squaresFrame[i][j].setDown(squaresFrame[i + 1][j]);
                    }
                }
            }
        }
    }

    private static boolean hasDown(Square[][] squaresFrame, int i, int j) {
        try {
            return squaresFrame[i + 1][j] != null;
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean hasUp(Square[][] squaresFrame, int i, int j) {
        try {
            return squaresFrame[i - 1][j] != null;
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean hasRight(Square[][] squaresFrame, int i, int j) {
        try {
            return squaresFrame[i][j + 1] != null;
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean hasLeft(Square[][] squaresFrame, int i, int j) {
        try {
            return squaresFrame[i][j - 1] != null;
        } catch (Exception e) {
            return false;
        }
    }


}


