package sample.controller;

import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import sample.model.Shape;
import sample.model.ShapesEnum;

import java.util.ArrayList;
import java.util.HashMap;

public class ModelAccess {

    private static MainController mainController;

    private static HashMap<ShapesEnum, Integer> squaresPerShape = new HashMap<>();
    private Shape selectedShape;
    private ArrayList<GridPane> shapeGrids = new ArrayList<>();
    private GridPane selectedShapeGrid;
    private HashMap<Integer, ImageView> mainGridImages = new HashMap<>();
    //TODO should be dynamic size
    private boolean[] highlightedSquares = new boolean[45];
    private int selectedShapeIndex = -1 ;

    public static void initialize() {
        squaresPerShape.put(ShapesEnum.L, 4);
        squaresPerShape.put(ShapesEnum.sL, 3);
        squaresPerShape.put(ShapesEnum.sI, 2);
        squaresPerShape.put(ShapesEnum.I, 3);
        squaresPerShape.put(ShapesEnum.z, 4);
    }


    public static MainController getMainController() {
        return mainController;
    }

    public static void setMainController(MainController mainController) {
        ModelAccess.mainController = mainController;
    }

    public static int getSquaresCount(ShapesEnum type) {
        return squaresPerShape.get(type);
    }

    public void setSelectedShape(Shape selectedShape) {
//        System.out.print(selectedShape.toString());
        this.selectedShape = selectedShape;
    }

    public Shape getSelectedShape() {
        return selectedShape;
    }

    public void addShape(GridPane shape) {
        shapeGrids.add(shape);
    }

    public void setSelectedShapeGrid(GridPane shape) {
        selectedShapeGrid = shape;
    }

    public GridPane getSelectedShapeGrid() {
        return selectedShapeGrid;
    }

    public void resetMainGridImages() {
        mainGridImages.clear();
    }

    public void addMainGridImage(int i, int j, ImageView imageView) {
        mainGridImages.put(i * 10 + j, imageView);
    }

    public void highlightSquares(ArrayList<Integer> availableMoves) {
        availableMoves.forEach(integer -> {
            ImageView imageView = mainGridImages.get(integer);
            //  if (!highlightedSquares[integer]) {
            imageView.setFitHeight(imageView.getFitHeight() - 7);
            imageView.setFitWidth(imageView.getFitWidth() - 7);
            highlightedSquares[integer] = true;
            // }
        });
    }

    public void resetHighlightedSquares() {
        //TODO dynamic size
        mainGridImages.forEach((integer, imageView) -> {
            if(highlightedSquares[integer]){
                imageView.setFitHeight(imageView.getFitHeight() + 7);
                imageView.setFitWidth(imageView.getFitWidth() + 7);
                highlightedSquares[integer] = false;
            }
        });

    }

    public void setSelectedShapeIndex(int finalI) {
        selectedShapeIndex = finalI;
    }

    public int getSelectedShapeIndex() {
        return selectedShapeIndex;
    }
}
