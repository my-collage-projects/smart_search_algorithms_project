package sample.controller;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ConfirmBox {

    static boolean Answer;

    public static boolean display(String title , String message){
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(300);
        window.setMinHeight(250);

        Label label1= new Label();
        label1.setText(message);

        //Create two buttons
        Button yesButton = new Button("yes");
        Button noButton = new Button("No");

        yesButton.setOnAction(e -> {
            Answer = true;
            window.close();
        });
        noButton.setOnAction(e -> {
            Answer = false;
            window.close();
        });

        VBox layout = new VBox(20);
        layout.getChildren().addAll(label1 , yesButton,noButton);
        layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();


        return Answer;



    }


}
