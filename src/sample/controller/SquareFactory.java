package sample.controller;

import sample.MainApp;
import sample.model.*;

public class SquareFactory {
    public static Square buildSquare(String input){
        if (input.equals("sn")) {
//            System.out.println("L/R/Up/Down");
//            Scanner scanner = new Scanner(System.in);
            String type = MainApp.readInputLine();
            return new SnakeSquare(SquaresEnum.valueOf(type));
        } else if (input.equals("st")) {
            return new StoneSquare();
        } else if (input.equals("g")) {
            return new GlassSquare();
        } else {
            return null;
        }
    }

}
