package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import sample.MainApp;
import sample.model.Puzzle;
import sample.model.Shape;
import sample.model.SquaresEnum;
import sample.view.StyleConstants;
import sample.view.ViewFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class MainController extends AbstractController implements Initializable {

    @FXML
    private ChoiceBox<?> choiceBoxLevel;

    @FXML
    private Button btnPlay;

    @FXML
    void actionHandler(ActionEvent event) {
        if (event.getSource().equals(btnPlay)) {
       /*     FXMLLoader loader;
            Parent parent = null;
            Scene scene;
            try {
                loader = new FXMLLoader(getClass().getResource("../view/GameLayout.fxml"));
                loader.setController(new GameController(getModelAccess(),5,1));
                parent = loader.load();
            } catch (IOException e) {
                System.out.println("IO Exception");
            }
            scene = new Scene(parent);*/
            Scene scene = ViewFactory.defaultFactory.getGameScene(5, 1);
            MainApp.setScene(scene);
        }
    }

    public MainController(ModelAccess modelAccess) {
        super(modelAccess);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        StyleConstants.initialize();
        ModelAccess.initialize();


    }


}
