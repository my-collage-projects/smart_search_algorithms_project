package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.controller.ConfirmBox;
import sample.view.ViewFactory;

import javax.naming.OperationNotSupportedException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MainApp extends Application {

    static Stage MainStage;

    static BufferedReader reader;
    private static int readerCounter;

    public static void startMainView() {
        try {
            Scene scene = ViewFactory.defaultFactory.getMainScene();
            MainStage.setScene(scene);
            MainStage.show();
            MainStage.setOnCloseRequest(event -> {
                event.consume();
                closeWindow(MainStage);
            });
        } catch (OperationNotSupportedException e) {
            e.printStackTrace();
        }
    }

    private static void closeWindow(Stage window) {
        if(ConfirmBox.display("Title" , "Sure you want to close ?")){
            //TODO : must save data before closing
            window.close();
        }
    }

    public static String readInputLine(){
        try {
            readerCounter++;
            String line = reader.readLine();
            System.err.println(line);
            return line;
        } catch (IOException exception) {
            System.err.println("error reading at line:" + readerCounter);
            return null;
        }
    }

    public static void setScene(Scene scene) {
        MainStage.setScene(scene);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        MainStage = primaryStage;

        primaryStage.setHeight(505);
        primaryStage.setWidth(690);
       /* new Thread(() -> {
            while (true) {
                System.out.println("width = " + primaryStage.getWidth());
                System.out.println("Height = " + primaryStage.getHeight());
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();*/
        startMainView();
    }


    public static void loadLevel(int levelNumber) {
        File inputFile = new File("level" + levelNumber + ".txt");
        try {

            if(inputFile.exists()){
                reader = new BufferedReader(new FileReader(inputFile));
            }
            else {
                inputFile.createNewFile();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
